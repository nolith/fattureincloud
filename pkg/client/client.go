package client

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/nolith/fattureincloud/pkg/api"
)

type RichiestaGenerica interface {
	Info(context.Context) (*api.Info, error)
}

// Questo set di funzioni agisce su diverse tipologie di documenti, identificate dalla variabile {tipo-doc}.
// I valori che possono essere assunti da {tipo-doc} sono i seguenti: fatture, ricevute, preventivi, ordini, ndc, proforma, rapporti, ordforn e ddt.
// 	Per chiarezza si specifica che "ndc" identifica le note di credito, "ordini" gli ordini a cliente e "ordforn" gli ordini a fornitore.
type DocumentiEmessi interface {
	NuovoRapporto(context.Context, *api.DocNuovoRequest) (*api.DocNuovoResponse, error)
	NuovoProforma(context.Context, *api.DocNuovoRequest) (*api.DocNuovoResponse, error)
}

type FullClient interface {
	RichiestaGenerica
	DocumentiEmessi
}

const (
	API_URI string = "https://api.fattureincloud.it/v1"
)

type client struct {
	uid    string
	apiKey string
	client *http.Client
}

func New(uid, api_key string) FullClient {
	return &client{
		uid:    uid,
		apiKey: api_key,
		client: http.DefaultClient,
	}
}

func (c *client) do(ctx context.Context, path string, params api.RichiestaAutorizzata, output interface{}) error {
	params.SetCredentials(c.uid, c.apiKey)

	pr, pw := io.Pipe()
	enc := json.NewEncoder(pw)

	timeoutCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	req, err := http.NewRequest("POST", API_URI+path, pr)
	if err != nil {
		return err
	}

	go func() {
		defer pw.Close()

		if err := enc.Encode(params); err != nil {
			log.Print(err)
		}
	}()

	res, err := c.client.Do(req.WithContext(timeoutCtx))
	if err != nil {
		return err
	}
	defer res.Body.Close()

	dec := json.NewDecoder(res.Body)
	return dec.Decode(output)
}

func (c *client) Info(ctx context.Context) (*api.Info, error) {
	r := api.Richiesta{}
	info := api.Info{}

	err := c.do(ctx, "/richiesta/info", &r, &info)
	if err != nil {
		return nil, err
	}

	return &info, info.GetError()
}

func (c *client) NuovoRapporto(ctx context.Context, doc *api.DocNuovoRequest) (*api.DocNuovoResponse, error) {
	resp := &api.DocNuovoResponse{}

	err := c.do(ctx, "/rapporti/nuovo", doc, resp)
	if err != nil {
		return nil, err
	}

	return resp, resp.GetError()
}

func (c *client) NuovoProforma(ctx context.Context, doc *api.DocNuovoRequest) (*api.DocNuovoResponse, error) {
	resp := &api.DocNuovoResponse{}

	err := c.do(ctx, "/proforma/nuovo", doc, resp)
	if err != nil {
		return nil, err
	}

	return resp, resp.GetError()
}
