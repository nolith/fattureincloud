package api

type Lingua string

const(
	Italiano Lingua = "it"
	Inglese Lingua = "en"
	Tedesco Lingua = "de"
)


type DocNuovoResponse struct {
	Risposta
	NewID int `json:"new_id"`
	Token string `json:"token"`
}

type DocNuovoRequest struct {
	Richiesta
	// IDCliente: Identificativo univoco del cliente (serve per creare un collegamento tra il documento e un cliente in anagrafica; se nullo, il documento non viene collegato a nessun cliente già esistente in anagrafica; se mancante, viene fatto il collegamento con piva o cf) [solo con tipo!="ordforn"],
	IDCliente string `json:"id_cliente,omitempty"`
	// IDFornitore: Identificativo univoco del fornitore (serve per creare un collegamento tra il documento e un fornitore in anagrafica; se nullo, il documento non viene collegato a nessun fornitore già esistente in anagrafica; se mancante, viene fatto il collegamento con piva o cf) [solo con tipo="ordforn"],
	IDFornitore string `json:"id_fornitore,omitempty"`
	// Nome o ragione sociale del cliente/fornitore
	Nome string `json:"nome"`
// indirizzo_via (string, opzionale): Indirizzo del cliente,
// indirizzo_cap (string, opzionale): CAP del cliente/fornitore,
// indirizzo_citta (string, opzionale): Città (comune) del cliente/fornitore,
// indirizzo_provincia (string, opzionale): Provincia del cliente/fornitore,
// indirizzo_extra (string, opzionale): Note extra sull'indirizzo,
// paese (string, opzionale): Paese (nazionalità) del cliente/fornitore,
// paese_iso (string, opzionale): [Solo se "paese" non è valorizzato] Codice ISO del paese (nazionalità) del cliente/fornitore (formato ISO alpha-2) in alternativa al parametro "paese",
	// Lingua del documento (sigla) = ['it' o 'en' o 'de'],
	Lingua Lingua `json:"lingua,omitempty"`
	// PIva: Partita IVA cliente/fornitore; viene utilizzata per ricercare e collegare il cliente/fornitore in anagrafica se non specificato il parametro IDCliente/IDFornitore (in caso di doppioni viene collegato solo un soggetto),
	PIva string `json:"piva,omitempty"`
// cf (string, opzionale): Codice fiscale cliente/fornitore; viene utilizzato per ricercare e collegare il cliente/fornitore in anagrafica se non specificati i parametri id_cliente/id_fornitore e piva (in caso di doppioni viene collegato solo un soggetto),
	// Autocompila_Anagrafica: Se "true", completa i dati anagrafici della fattura con quelli presenti nell'anagrafica cliente/fornitore (sovrascrivendo quelli presenti), effettuando la ricerca tramite i campi id_cliente/id_fornitore, piva e cf (in quest'ordine),
	AutocompilaAnagrafica bool `json:"autocompila_anagrafica"`
// salva_anagrafica (boolean, opzionale): Se "true", aggiorna l'anagrafica clienti/fornitori con i dati anagrafici della fattura: se il cliente/fornitore non esiste viene creato, mentre se il cliente/fornitore esiste già i dati vengono aggiornati; il cliente/fornitore viene ricercato tramite i campi id_cliente/id_fornitore, piva e cf (in quest'ordine),
// numero (string, opzionale): Numero (e serie) del documento; se mancante viene utilizzato il successivo proposto per la serie principale; se viene specificata solo la serie (stringa che inizia con un carattere non numerico) viene utilizzato il successivo per quella serie; per i ddt agisce anche sul campo "ddt_numero" e non accetta una serie,
	// Data: Data di emissione del documento (per i ddt agisce anche sul campo "ddt_data")
	Data *Time `json:"data,omitempty"`
// valuta (string, opzionale): Valuta del documento e degli importi indicati,
// valuta_cambio (double, opzionale): Tasso di cambio EUR/{valuta} [Se non specificato viene utilizzato il tasso di cambio odierno],
// prezzi_ivati (boolean, opzionale): Specifica se i prezzi da utilizzare per il calcolo del totale documento sono quelli netti, oppure quello lordi (comprensivi di iva),
// rivalsa (double, opzionale): [Non presente in ddt e ordforn] Percentuale rivalsa INPS,
// cassa (double, opzionale): [Non presente in ddt e ordforn] Percentuale cassa previdenziale,
// rit_acconto (double, opzionale): [Non presente in ddt e ordforn] Percentuale ritenuta d'acconto,
// imponibile_ritenuta (double, opzionale): [Non presente in ddt e ordforn] Imponibile della ritenuta d'acconto (percentuale sul totale),
// rit_altra (double, opzionale): [Non presente in ddt e ordforn] Percentuale altra ritenuta (ritenuta previdenziale, Enasarco etc.),
// marca_bollo (double, opzionale): [Non presente in ddt e ordforn] Valore della marca da bollo (0 se non presente),
	// OggettoVisibile: [Non presente in ddt] Oggetto mostrato sul documento
	OggettoVisibile string `json:"oggetto_visibile"`
// oggetto_interno (string, opzionale): [Non presente in ddt] Oggetto (per organizzazione interna),
// centro_ricavo (string, opzionale): [Non presente in ddt e ordforn] Centro ricavo,
// centro_costo (string, opzionale): [Solo in ordforn] Centro di costo,
// note (string, opzionale): [Non presente in ddt] Note (in formato HTML),
// nascondi_scadenza (boolean, opzionale): [Non presente in ddt] Nasconde o mostra la scadenza sul documento,
// ddt (boolean, opzionale): [Non presente in ndc e ordforn] Indica la presenza di un DDT incluso nel documento (per i ddt è sempre true),
// ftacc (boolean, opzionale): [Solo se tipo=fatture] Indica la presenza di una fattura accompagnatoria inclusa nel documento,
// id_template (string, opzionale): [Solo se tipo!=ddt] Identificativo del template del documento [Se non specificato o inesistente, viene utilizzato quello di default],
// ddt_id_template (string, opzionale): [Solo se ddt=true] Identificativo del template del ddt [Se non specificato o inesistente, viene utilizzato quello di default],
// ftacc_id_template (string, opzionale): [Solo se ftacc=true] Identificativo del template della fattura accompagnatoria [Se non specificato o inesistente, viene utilizzato quello di default],
	// MostraInfoPagamento: [Non presente in ddt e ndc] Mostra o meno le informazioni sul metodo di pagamento sul documento
	MostraInfoPagamento bool `json:"mostra_info_pagamento"`
	// MetodoPagamento: [Solo se MostraInfoPagamento=true] Nome del metodo di pagamento
	MetodoPagamento string `json:"metodo_pagamento,omitempty"`
	// MetodoTitolo1: [Solo se MostraInfoPagamento=true] Titolo della riga 1 del metodo di pagamento
	MetodoTitolo1 string `json:"metodo_titolo1,omitempty"`
	// MetodoDesc1: [Solo se MostraInfoPagamento=true] Descrizione della riga 1 del metodo di pagamento
	MetodoDesc1 string `json:"metodo_desc1,omitempty"`
	// MetodoTitolo2: [Solo se MostraInfoPagamento=true] Titolo della riga 2 del metodo di pagamento
	MetodoTitolo2 string `json:"metodo_titolo2,omitempty"`
	// MetodoDesc2: [Solo se MostraInfoPagamento=true] Descrizione della riga 2 del metodo di pagamento
	MetodoDesc2 string `json:"metodo_desc2,omitempty"`
	// MetodoTitolo3: [Solo se MostraInfoPagamento=true] Titolo della riga 3 del metodo di pagamento
	MetodoTitolo3 string `json:"metodo_titolo3,omitempty"`
	// MetodoDesc3: [Solo se MostraInfoPagamento=true] Descrizione della riga 3 del metodo di pagamento
	MetodoDesc3 string `json:"metodo_desc3,omitempty"`
	// MetodoTitolo4: [Solo se MostraInfoPagamento=true] Titolo della riga 4 del metodo di pagamento
	MetodoTitolo4 string `json:"metodo_titolo4,omitempty"`
	// MetodoDesc4: [Solo se MostraInfoPagamento=true] Descrizione della riga 4 del metodo di pagamento
	MetodoDesc4 string `json:"metodo_desc4,omitempty"`
	// MetodoTitolo5: [Solo se MostraInfoPagamento=true] Titolo della riga 5 del metodo di pagamento
	MetodoTitolo5 string `json:"metodo_titolo5,omitempty"`
	// MetodoDesc5: [Solo se MostraInfoPagamento=true] Descrizione della riga 5 del metodo di pagamento
	MetodoDesc5 string `json:"metodo_desc5,omitempty"`
// mostra_totali (string, opzionale): [Solo per preventivi, rapporti e ordforn] Nasconde o mostra la scadenza sul documento = ['tutti' o 'netto' o 'nessuno'],
// mostra_bottone_paypal (boolean, opzionale): [Solo per ricevute, fatture, proforma, ordini] Mostra il bottone "Paga con Paypal",
// mostra_bottone_bonifico (boolean, opzionale): [Solo per ricevute, fatture, proforma, ordini] Mostra il bottone "Paga con Bonifico Immediato",
// mostra_bottone_notifica (boolean, opzionale): [Solo per ricevute, fatture, proforma, ordini] Mostra il bottone "Notifica pagamento effettuato",
	// ListaArticoli: Lista degli articoli/righe del documento
	ListaArticoli []DocNuovoArticolo `json:"lista_articoli"`
	// ListaPagamenti: [Non presente in preventivi, ddt e ordforn] Lista delle tranches di pagamento
	ListaPagamenti []DocNuovoPagamento `json:"lista_pagamenti"`
// ddt_numero (string, opzionale): [Se ddt=true] Numero del ddt (se tipo="ddt" corrisponde al campo "numero") [Se non specificato viene autocompletato],
// ddt_data (date, opzionale): [Se ddt=true] Data del ddt Numero del ddt [Obbligatoria solo se e solo se il documento non è un ddt ma ddt=true],
// ddt_colli (string, opzionale): [Se ddt/ftacc=true] Numero di colli specificato nel ddt,
// ddt_peso (string, opzionale): [Se ddt/ftacc=true] Peso specificato nel ddt,
// ddt_causale (string, opzionale): [Se ddt/ftacc=true] Causale specificata nel ddt,
// ddt_luogo (string, opzionale): [Se ddt/ftacc=true] Luogo di spedizione specificato nel ddt,
// ddt_trasportatore (string, opzionale): [Se ddt/ftacc=true] Dati trasportatore specificati nel ddt,
// ddt_annotazioni (string, opzionale): [Se ddt/ftacc=true] Annotazioni specificate nel ddt,
// PA (boolean, opzionale): [Solo per fatture e ndc elettroniche] Indica se il documento è nel formato FatturaPA; se "true", vengono presi in considerazione tutti i successivi campi con prefisso "PA_", con eventuali eccezioni (se non valorizzati, vengono utilizzati i valori di default),
// PA_tipo_cliente (string, opzionale): [Solo se PA=true] Indica la tipologia del cliente: Pubblica Amministrazione ("PA") oppure privato ("B2B") = ['PA' o 'B2B'],
// PA_tipo (string, opzionale): [Solo se PA=true] Tipo di documento a cui fa seguito la fattura/ndc in questione = ['ordine' o 'convenzione' o 'contratto' o 'nessuno'],
// PA_numero (string, opzionale): [Solo se PA=true] Numero del documento a cui fa seguito la fattura/ndc in questione,
// PA_data (date, opzionale): [Solo se PA=true] Data del documento a cui fa seguito la fattura/ndc in questione,
// PA_cup (string, opzionale): [Solo se PA_tipo_cliente=PA] Codice Unitario Progetto,
// PA_cig (string, opzionale): [Solo se PA_tipo_cliente=PA] Codice Identificativo della Gara,
// PA_codice (string, opzionale): [Solo se PA=true] Codice Ufficio della Pubblica Amministrazione o Codice Destinatario,
// PA_pec (string, opzionale): [Solo se PA_tipo_cliente=B2B] Indirizzo PEC del destinatario, utilizzato in assenza di Codice Destinatario,
// PA_esigibilita (string, opzionale): [Solo se PA=true] Esigibilità IVA e modalità di versamento (I=immediata, D=differita, S=split payment, N=non specificata) = ['I' o 'D' o 'S' o 'N'],
// PA_modalita_pagamento (string, opzionale): [Solo se PA=true] Modalità di pagamento (vedi tabella codifiche sulla documentazione ufficiale),
// PA_istituto_credito (string, opzionale): [Solo se PA=true] Nome dell'istituto di credito,
// PA_iban (string, opzionale): [Solo se PA=true] Codice IBAN del conto corrente del beneficiario,
// PA_beneficiario (string, opzionale): [Solo se PA=true] Beneficiario del pagamento,
// extra_anagrafica (DocNuovoExtraAnagrafica, opzionale): Informazioni anagrafiche aggiuntive da associare al cliente/fornitore [solo se salva_anagrafica=true],
// split_payment (boolean, opzionale): [Solo per fatture, ndc e proforma NON elettroniche] Specifica se il documento applica lo split payment
}

type DocNuovoArticolo struct {
	// ID: Identificativo del prodotto (se nullo o mancante, la registrazione non viene collegata a nessun prodotto presente nell'elenco prodotti)
	ID string `json:"id,omitempty"`
	// Codice prodotto
	Codice string `json:"codice"`
	// Nome articolo
	Nome string `json:"nome"`
// um (string, opzionale): Unità di misura per il prodotto,
// quantita (double, opzionale): Quantità di prodotto,
// descrizione (string, opzionale): Descrizione del prodotto,
	// Categoria del prodotto (utilizzata per il piano dei conti)
	Categoria string `json:"categoria"`
	// PrezzoNetto unitario (IVA esclusa) [Obbligatorio se prezzi_netti!=true],
	PrezzoNetto float64 `json:"prezzo_netto,omitempty"`
	// PrezzoLordo unitario (comprensivo di IVA) [Obbligatorio se prezzi_ivati=true]
	PrezzoLordo float64 `json:"prezzo_lordo,omitempty"`
	// CodIva: Codice aliquota IVA (ottenibili con il parametro "lista_iva" della funzione "/info/account")
	CodIva int `json:"cod_iva"`
	// Tassabile indica se l'articolo è imponibile,
	Tassabile *bool `json:"tassabile,omitempty"`
// sconto (double, opzionale): Sconto (percentuale),
// applica_ra_contributi (boolean, opzionale): Indica se a questo articolo vengono applicate ritenute e contributi,
	// Ordine dell'articolo nel documento (ordinamento ascendente da 0 in poi)
	Ordine int `json:"ordine,omitempty"`
// sconto_rosso (integer, opzionale): Se vale 1, evidenzia in rosso l'eventuale sconto in fattura = ['0' o '1'],
// in_ddt (boolean, opzionale): Indica se l'articolo è incluso nel ddt (se presente un ddt allegato, altrimenti non è significativo),
	// Magazzino indica se viene movimentato il magazzino (true: viene movimentato; false: non viene movimentato) [Non influente se il prodotto non è collegato all'elenco prodotti, oppure la funzionalità magazzino è disattivata]
	Magazzino bool `json:"magazzino"`
}

type DocNuovoPagamento struct {
	// DataScadenza: Data di scadenza del pagamento
	DataScadenza Time `json:"data_scadenza"`
	// Importo: Importo del pagamento (se vale 0 la tranche di pagamento non viene inserita; se vale "auto" e la tranche è una sola viene completato automaticamente)
	Importo string `json:"importo"`

	// Metodo: Metodo di pagamento = ['not' o 'rev' o il nome del conto] ('not' indica che non è stato saldato, 'rev' che è stato stornato; se non esiste un conto con il nome specificato viene creato un nuovo conto in FIC)
	Metodo string `json:"metodo"`
	// DataSaldo: Data del saldo dell'importo indicato [Obbligatorio se metodo!="not"]
	DataSaldo *Time `json:"data_saldo,omitempty"`
}

// DocNuovoExtraAnagrafica {
// mail (string, opzionale): Indirizzo di posta elettronica,
// tel (string, opzionale): Recapito telefonico,
// fax (string, opzionale): Numero fax
// } 
