package api

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestDateFormat(t *testing.T) {
	tests := []struct {
		name       string
		date       time.Time
		expected   string
		shouldFail bool
	}{
		{
			name:     "a date",
			date:     time.Date(2019, 1, 18, 0, 0, 0, 0, time.UTC),
			expected: "\"18/01/2019\"",
		},
		{
			name:     "zero padded date",
			date:     time.Date(2019, 1, 2, 0, 0, 0, 0, time.UTC),
			expected: "\"02/01/2019\"",
		},
		{
			name:     "zero",
			expected: "\"\"",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ficTime := Time(test.date)

			marshaled, err := ficTime.MarshalJSON()
			if test.shouldFail {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				require.Equal(t, test.expected, string(marshaled))
			}
		})
	}
}

func TestJsonOmitempty(t *testing.T) {
	type obj struct {
		Date *Time `json:"date,omitempty"`
	}

	empty, err := json.Marshal(obj{})
	require.NoError(t, err)
	require.NotContains(t, string(empty), "date")

	now := Time(time.Now())
	jsonObj, err := json.Marshal(obj{&now})
	require.NoError(t, err)
	require.Contains(t, string(jsonObj), "date")
}
