package api

import "errors"

type RichiestaAutorizzata interface {
	SetCredentials(apiUID, apiKey string)
}

type Richiesta struct{
	APIUID   string `json:"api_uid"`
	APIKey    string `json:"api_key"`
}

func (r *Richiesta) SetCredentials(apiUID, apiKey string) {
	r.APIUID = apiUID
	r.APIKey = apiKey
}

type Risposta struct {
	Error *string `json:"error"`
	ErrorCode *int `json:"error_code"`
	Success bool `json:"success"`
}

type Info struct {
	*Risposta
	Messaggio string `json:"messaggio"`
	LimiteBreve string `json:"limite_breve"`
	LimiteMedio string `json:"limite_medio"`
	LimiteLungo string `json:"limite_lungo"`

}

func (r *Risposta) IsError() bool {
	return !r.Success
}


func (r *Risposta) GetError() error {
	if r.IsError() {
		if r.Error != nil {
			return errors.New(*r.Error)
		}

		return errors.New("Errore sconosciuto")
	}

	return nil
}
