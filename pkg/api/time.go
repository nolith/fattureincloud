package api

import (
	"fmt"
	"time"
)

const layout string = "02/01/2006" // dd/MM/yyyy

// Time rappresentazione delle date nel formato FattureInCloud
type Time time.Time

// MarshalJSON formatta le date come dd/MM/yyyy
func (t Time) MarshalJSON() ([]byte, error) {
	asString := time.Time(t).Format(layout)
	return []byte(fmt.Sprintf("%q", asString)), nil
}
