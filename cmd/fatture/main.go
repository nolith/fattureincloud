package main

import (
	"fmt"
	"os"
	"context"

	"gitlab.com/nolith/fattureincloud/pkg/client"
)

func main() {
	fmt.Println("Creazione rapporto base")

	c := client.New(os.Getenv("API_UID"), os.Getenv("API_KEY"))

	i, err := c.Info(context.Background())
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v\n%+v\n", i, i.Risposta)

}
